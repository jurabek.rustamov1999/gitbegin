package io.jurabek;

class ProductModel{
   private String productName;
   private int soldProduct;
   private int remainingProduct;
   private double price;

   public ProductModel(String productName, int soldProduct, int remainingProduct, double price) {
      this.productName = productName;
      this.soldProduct = soldProduct;
      this.remainingProduct = remainingProduct;
      this.price = price;
   }

   public String getProductName() {
      return productName;
   }

   public void setProductName(String productName) {
      this.productName = productName;
   }

   public int getSoldProduct() {
      return soldProduct;
   }

   public void setSoldProduct(Integer soldProduct) {
      this.soldProduct = soldProduct;
   }

   public int getRemainingProduct() {
      return remainingProduct;
   }

   public void setRemainingProduct(Integer remainingProduct) {
      this.remainingProduct = remainingProduct;
   }

   public double getPrice() {
      return price;
   }

   public void setPrice(Double price) {
      this.price = price;
   }

   @Override
   public String toString(){
      return this.productName + ", " + this.soldProduct + ", " + this.remainingProduct + ", " + this.price;
   }
}

