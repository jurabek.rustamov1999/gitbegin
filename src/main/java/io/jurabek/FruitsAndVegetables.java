package io.jurabek;

public class FruitsAndVegetables extends ProductModel {

    private Integer discount;

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public FruitsAndVegetables(String productName, Integer soldProduct,
                               Integer remainingProduct, Double price, Integer discount) {
        super(productName, soldProduct, remainingProduct, price);

        this.discount = discount;
    }
}
