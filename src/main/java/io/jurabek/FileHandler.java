package io.jurabek;

import java.io.*;
import java.util.LinkedList;

public class FileHandler {

    String getFile(int choice){

        String putFile;
        switch (choice){
            case 1:
                putFile = "Beverages.csv";
                break;
            case 2:
                putFile = "BreadAndPastries.csv";
                break;
            case 3:
                putFile = "FruitsAndVegetables.csv";
                break;
            case 4:
                putFile = "MeatProducts.csv";
                break;
            case 5:
                putFile = "MilkProducts.csv";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + choice);
        }
        return putFile;
    }

    public LinkedList<ProductModel> readFile(int choice){

        LinkedList<ProductModel> productModel = new LinkedList<>();

        String lineRead;

        String[] splitLine;

        ProductModel productModel1;

        try(BufferedReader reader = new BufferedReader(new FileReader(getFile(choice)))) {

            lineRead = reader.readLine();

            while (lineRead != null){

                splitLine = lineRead.split(", ");

                productModel1 = new Beverages(splitLine[0], Integer.parseInt(splitLine[1]),
                        Integer.parseInt(splitLine[2]), Double.parseDouble(splitLine[3]),
                        Integer.parseInt(splitLine[4]));

                productModel.add(productModel1);

                lineRead = reader.readLine();

            }
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        return productModel;
    }
}
