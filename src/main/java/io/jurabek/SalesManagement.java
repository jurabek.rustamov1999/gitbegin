package io.jurabek;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

public class SalesManagement {

    final private Scanner reader = new Scanner(System.in);

    public int getIntInput() {

        int choice = 0;

        while (choice == 0){
            try {
                System.out.print("Enter your choice please: ");
                choice = reader.nextInt();
                if (choice == 0)
                    throw new InputMismatchException();
                reader.nextLine();
            }
            catch (InputMismatchException e){
                reader.nextLine();
                System.out.println("\nERROR INVALID INPUT. Please try again: ");
            }
        }

        return choice;
    }

    private void catalogProducts(){
        System.out.println("\n1 | Beverages");
        System.out.println("2 | Bread and pastries");
        System.out.println("3 | Fruit and Vegetables");
        System.out.println("4 | Meat products");
        System.out.println("5 | Milk products");
    }

    public int getChoice(){

        int choice;

        System.out.println("\nWELCOME TO shop products LOVE");
        System.out.println("****************************");
        System.out.println("1 | Go for products");
        System.out.println("2 | Go to the basked");
        System.out.println("0 | Quite the shop");

        choice = getIntInput();

        return choice;
    }

    public LinkedList<ProductModel> goForProducts(LinkedList<ProductModel> productModel){

        catalogProducts();

        int choice = getIntInput();

        while (choice < 1 || choice > 5){
            System.out.print("\nInvalid Club ID. Please try again: ");
            choice = getIntInput();
        }

        switch (choice){
            case 1 -> {

            }
        }

        FileHandler file = new FileHandler();

        return file.readFile(choice);
    }
}