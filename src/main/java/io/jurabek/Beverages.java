package io.jurabek;

public class Beverages extends ProductModel{

    private int discount;

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Beverages(String productName, int soldProduct,
                     int remainingProduct, double price, int discount) {
        super(productName, soldProduct, remainingProduct, price);

        this.discount = discount;
    }
}
